{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Aqueous geothermometry examples\n",
    "\n",
    "### Run this section first\n",
    "Loads necessary environment for R (boilerplate preamble stuff)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%env R_HOME=/Applications/anaconda/envs/py36andR/lib/R\n",
    "import rpy2.rinterface\n",
    "%load_ext rpy2.ipython"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "source(\"/Users/graysonboyer/AqGeothermometry/scripts/R_thermo_univariant.r\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "### Example 1: Silica in equilibrium with quartz\n",
    "\n",
    "You are in Yellowstone National Park sampling water from five different hot springs and measure dissolved silica log activities of -2.71, -2.22, -1.84, -2.42, and -2.35. You figure these activities might indicate water-rock interactions with quartz in the hot subsurface. Quartz in equilibrium with aqueous silica can be written as:\n",
    "\n",
    "$$ \\mathop{\\rm{SiO_{2(cr)}}}\\limits_{(quartz)} \\rightleftharpoons \\rm{SiO_{2(aq)}} $$\n",
    "\n",
    "The logK of this reaction can be found from: \n",
    "\n",
    "\\begin{align}\n",
    "\\log K & = \\log a\\rm{SiO_{2(aq)}} - \\log a\\mathop{\\rm{SiO_{2(cr)}}}\\limits_{(quartz)} \\\\\n",
    "\\log K & = \\log a\\rm{SiO_{2(aq)}} - 0 \\\\\n",
    "\\log K & = \\log a\\rm{SiO_{2(aq)}}\n",
    "\\end{align}\n",
    "\n",
    "What subsurface temperatures might be expected to produce observed activities of dissolved silica? We can also try pressures of 200 and 400 bars to see how changes in pressure affect these predicted temperatures. First, let's check if quartz and aqueous silica are available in the thermodynamic database:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# view the first few entries of the 'OBIGT' thermodynamic database\n",
    "head(thermo$obigt)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# help(thermo) # uncomment this to view thermodynamic database documentation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "library(dplyr) # load R library to select, filter, etc.\n",
    "\n",
    "### search aqueous, liquid, gaseous, and crystalline entries\n",
    "(thermo$obigt\n",
    "    %>% filter(state == \"cr\") # \"aq\", \"liq\", \"cr\", or \"cr_Berman\"\n",
    "    %>% select(name, state) # select these columns\n",
    "    %>% filter(substr(name, 1, 1) == \"q\") # starts with letter...\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "\n",
    "# This should return quartz's entry in the thermodynamic database\n",
    "# Warnings should also appear about available phase transitions and a heat capacity (Cp) calculation\n",
    "# Note that quartz is available in cr_Berman!\n",
    "info(info(\"quartz\", \"cr_Berman\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "\n",
    "# look up properties of quartz calculated by Berman equations\n",
    "berman(\"quartz\", T=25, P=1, units = \"cal\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "\n",
    "# This should return aqueous silica's entry in the thermodynamic database\n",
    "# Warnings should also appear to say that SiO2 is available in many different forms\n",
    "info(info(\"SiO2\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "\n",
    "\n",
    "### User-specified parameters ###\n",
    "logKs <- -2.71   # logK\n",
    "pressures <- 200 # bars\n",
    "minT <- 1        # min T (degC)\n",
    "maxT <- 350      # max T (degC)\n",
    "\n",
    "species <- c(\"quartz\", \"SiO2\")  # chemical species\n",
    "phase <- c(\"cr_Berman\", \"aq\")   # can be aq, gas, liq, cr, or cr_Berman\n",
    "stoich <- c(-1, 1)              # rxn stoichiometry (negative reactants, positive products)\n",
    "\n",
    "# leave code below as-is\n",
    "# Loops through each logK\n",
    "for(logK in logKs){\n",
    "    result <- uc_solveT(logK, species, phase = phase, stoich = stoich, pressures = pressures, minT = minT, maxT = maxT)\n",
    "    print(paste0(\"logK: \", logK, \", Temperatures: \", result$out$T, \", for pressure: \", pressures, \" bars\"))\n",
    "    create_output_plot_T()\n",
    "}\n",
    "print(result$reaction)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "### Example 2: Silica in equilibrium with cristobalite\n",
    "\n",
    "The five hot spring water samples you obtained in Example 1 have dissolved silica log activities of -2.71, -2.22, -1.84, -2.42, and -2.35. Equilibrium with quartz in the hot subsurface might explain these values, but so might water-rock interactions with cristobalite. Cristobalite in equilibrium with aqueous silica can be written as:\n",
    "\n",
    "$$ \\mathop{\\rm{SiO}_{2(cr)}}\\limits_{(cristobalite)} \\rightleftharpoons \\rm{SiO}_{2(aq)} $$\n",
    "\n",
    "Like in the previous quartz example, the logK of this reaction can be found from the activity of aqueous silica: \n",
    "\n",
    "\\begin{align}\n",
    "\\log K & = \\log a\\rm{SiO}_{2(aq)}\n",
    "\\end{align}\n",
    "\n",
    "What temperature would be required to produce observed aqueous silica activities in equilibrium with cristobalite? Are these temperatures very different than the those obtained in Example 1? Assume a subsurface pressure of 200 bars. First, let's check if cristobalite is available in the thermodynamic database:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# This should return cristobalite's entry in the thermodynamic database\n",
    "# Warnings should also appear about available phase transitions and a heat capacity (Cp) calculation\n",
    "info(info(\"cristobalite\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "\n",
    "\n",
    "### User-specified parameters ###\n",
    "logKs <- ???                              # logK\n",
    "pressures <- ???                          # bars\n",
    "minT <- 1                                 # min T (degC)\n",
    "maxT <- 350                               # max T (degC)\n",
    "\n",
    "species <- ??? # chemical species\n",
    "phase   <- ??? # can be aq, gas, liq, cr, or cr_Berman\n",
    "stoich  <- ??? # rxn stoichiometry (negative reactants, positive products)\n",
    "\n",
    "# leave code below as-is\n",
    "# Loops through each logK\n",
    "for(logK in logKs){\n",
    "    result <- uc_solveT(logK, species, phase = phase, stoich = stoich, pressures = pressures, minT = minT, maxT = maxT)\n",
    "    print(paste0(\"logK: \", logK, \", Temperatures: \", result$out$T, \", for pressure: \", pressures, \" bars\"))\n",
    "    create_output_plot_T()\n",
    "}\n",
    "print(result$reaction)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "### Example 3: K-feldspar, kaolinite, and muscovite assemblage\n",
    "\n",
    "The five hot spring water samples we looked at in Examples 1 and 2 have dissolved silica log activities of -2.71, -2.22, -1.84, -2.42, and -2.35. This time, you decide to explore the possibility that these silica activities could indicate water-rock interactions with a K-feldspar, kaolinite, and muscovite mineral assemblage in the hot subsurface.\n",
    "\n",
    "\\begin{align}\n",
    "\\mathop{\\rm{K(AlSi_{3})O_{8}}}\\limits_{(K\\text{-}feldspar)} + \\mathop{\\rm{Al_{2}Si_{2}O_{5}(OH)_{4}}}\\limits_{(kaolinite)} & \\rightleftharpoons \\rm{H_{2}O} + \\rm{2SiO_{2(aq)}} + \\mathop{\\rm{KAl_{2}(AlSi_{3})O_{10}(OH)_{2}}}\\limits_{(muscovite)} \\\\\n",
    "\\end{align}\n",
    "\n",
    "The logK of this reaction can be written as:\n",
    "\n",
    "$$ \\log K = 2\\log a\\rm{SiO_{2(aq)}} $$\n",
    "\n",
    "What temperature would be required to produce observed aqueous silica activities in equilibrium with this mineral assemblage? Are these any different than the temperatures obtained in Examples 1 and 2? Assume a subsurface pressure of 200 bars. First, let's check if these minerals are in the database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# various warnings should also appear. These minerals are all available in cr_Berman form!\n",
    "print(info(info(\"K-feldspar\")))\n",
    "print(info(info(\"kaolinite\")))\n",
    "print(info(info(\"muscovite\")))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "\n",
    "### User-specified parameters ###\n",
    "logKs     <- ??? # obtain logKs from log aSiO2\n",
    "pressures <- ??? # bars\n",
    "minT      <- 1   # min T (degC)\n",
    "maxT      <- 350 # max T (degC)\n",
    "\n",
    "species <- ??? # chemical species\n",
    "phase   <- ??? # aq, gas, liq, cr, or cr_Berman\n",
    "stoich  <- ??? # reaction stoichiometry (negative means reactants, positive means products)\n",
    "\n",
    "\n",
    "# Leave code below as-is:\n",
    "# Loops through each logK\n",
    "for(logK in logKs){\n",
    "    result <- uc_solveT(logK, species, phase = phase, stoich = stoich, pressures = pressures, minT = minT, maxT = maxT)\n",
    "    print(paste0(\"logK: \", logK, \", Temperatures: \", result$out$T, \", for pressure: \", pressures, \" bars\"))\n",
    "    create_output_plot_T()\n",
    "}\n",
    "print(result$reaction)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "### Example 4: silica geothermometry in a serpentinizing system\n",
    "\n",
    "You are in the Oman Ophiolite and you just sampled two high pH (pH > 11) fluids seeping out from peridotite outcrops. You name these samples \"Gas Crack\" and \"Snail\". Back at the lab, you use mass spectrometry to find total Si dissolved in each sample:\n",
    "\n",
    "| Sample     | Total Si ($\\mu$molal)  |\n",
    "| ---------- |-----------------------:|\n",
    "| Gas Crack  |  1.38                  |\n",
    "| Snail      | 10.5                   |\n",
    "\n",
    "Like in the previous examples, you can use silica activities to infer the subsurface temperature where these fluids came from. However, quartz, cristobalite and the other silica-rich phases you encountered earlier rarely occur in peridotites either as a primary or a secondary mineral. Instead, during water-peridotite interactions, serpentinization occurs and the silica activity of the fluid can be buffered by both chrysotile (a mineral in the serpentine group) and brucite:\n",
    "\n",
    "\\begin{align}\n",
    "\\mathop{\\rm{Mg_{3}Si_{2}O_{5}(OH)_{4}}}\\limits_{(chrysotile)} + \\rm{H_{2}O} & \\rightleftharpoons \\mathop{\\rm{3Mg(OH)_{2}}}\\limits_{(brucite)} + 2\\rm{SiO_{2(aq)}} \\\\\n",
    "\\end{align}\n",
    "\n",
    "The logK of this reaction can be written as:\n",
    "\n",
    "$$ \\log K = 2\\log a\\rm{SiO_{2(aq)}} $$\n",
    "\n",
    "\n",
    "However, as the pH is highly alkaline, $\\rm{aSiO_{2(aq)}} \\neq$ total $\\rm{Si}$  in molality. $\\rm{SiO_{2(aq)}}$, the species needed to calculate temperatures using the reaction above, is one of the various forms of Si that exists in the fluid. At low to circumneutral pH, $\\rm{SiO_{2(aq)}}$ is the dominant species of Si and thus, it is usually assumed that $\\rm{aSiO_{2(aq)}}$ = total Si in molality at this pH range. This is not the case at alkaline condition and to determine $\\rm{SiO_{2(aq)}}$, a speciation tool is needed.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### *EQ3 aqueous speciation demo here*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After speciating these Oman Ophiolite samples in EQ3, we can see that the $\\log a\\rm{SiO_{2(aq)}}$ is -8.33 for \"Gas Crack\" and -7.23 for \"Snail\". Now try using silica geothermometry to infer last temperature of equilibration with chrysotile and brucite assuming a subsurface pressure of 200 bars.\n",
    "\n",
    "First, are chrysotile and brucite available in the thermodynamic database?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# This should return chrysotile's entry in the thermodynamic database\n",
    "# Warnings should also appear to say that chrysotile is available in cr or cr_Berman form\n",
    "info(info(\"chrysotile\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "# This should return brucite's entry in the thermodynamic database\n",
    "# Warnings should also appear to say that brucite is available in cr or cr_Berman form\n",
    "info(info(\"brucite\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%R\n",
    "\n",
    "\n",
    "### User-specified parameters ###\n",
    "logKs     <- ??? # obtain logKs from log aSiO2\n",
    "pressures <- ??? # bars\n",
    "minT      <- 1   # min T (degC)\n",
    "maxT      <- 350 # max T (degC)\n",
    "\n",
    "species <- ??? # chemical species\n",
    "phase   <- ??? # can be aq, gas, liq, cr, or cr_Berman\n",
    "stoich  <- ??? # rxn stoichiometry (negative reactants, positive products)\n",
    "\n",
    "\n",
    "# Leave code below as-is:\n",
    "# Loops through each logK\n",
    "for(logK in logKs){\n",
    "    result <- uc_solveT(logK, species, phase = phase, stoich = stoich, pressures = pressures, minT = minT, maxT = maxT)\n",
    "    print(paste0(\"logK: \", logK, \", Temperatures: \", result$out$T, \", for pressure: \", pressures, \" bars\"))\n",
    "    create_output_plot_T()\n",
    "}\n",
    "print(result$reaction)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:py36andR]",
   "language": "python",
   "name": "conda-env-py36andR-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
