About:
-

This notebook takes tabular water chemistry measurements, runs them through geochemical software called EQ3/6, then datamines charge balance values from output files. This is a collaborative effort between Arizona State University graduate students Grayson Boyer and Tucker Ely, and University of Washington Professor Mark Ghiorso as a part of the NSF-funded ENKI project to produce freely available geochemical tools.


Changelog:
-

- May 22, 2019: overhaul of file structure. Project should now be self-contained.
- May 21, 2019: transferred project to ENKI GitLab
- Feb 7, 2018: Uploaded most up-to-date version, 0.9.1, to GitHub

Acknowledgments:
-
Several bugs have been squashed thanks to reports by the following people:
* Dylan Gagler
* James Leong
* Kirt Robinson