#######################################################################################################
#########    Call_EQ3:  To build and execute EQ3 files in batches as per user constraints    ##########
############################      Tucker Ely, 20 May  2016    #########################################
#######################################################################################################


import os, re, sys, shutil, fileinput, getopt, argparse, csv, copy
import numpy as np
import pandas as pd
from itertools import *
from collections import *
from subprocess import * 
from time import *
import matplotlib
import matplotlib.pyplot as plt
from time import gmtime, strftime
from datetime import datetime



#################################################################
########################    Main    #############################
def main(data0, T, P, pH, fO2, Aqueous, Hetero, steps, x_axis, y_axis, multi_range, charge_bal, path=False): 	#	T, P, pH, Aqueous, Hetero, steps

	local_db_path = os.path.join(path, 'db')    #    Users local db
	""" prepare input data """
	### check for input mistakes
	input_error_check(T, P, pH, fO2, Aqueous, Hetero, steps, x_axis, y_axis, multi_range)
	
	### record input data as strings for eventual figure, as they well go on this become manipulated
	out_text = out(T, P, pH, fO2, Aqueous, Hetero, x_axis)

	### build list of lists
	multi_steps = 5 							#	number of steps to divide the multi_range into
	steps = int(steps)							#	make sure steps is an integer

	### Expand lists into steps lists based on values/ranges. this copies over the multi-list if it already exists.
	aq_sp_list, multi_list = process_lists(Aqueous, steps, multi_range, multi_steps)
	
	### These must come after the aq list processing above, so that their multi_list is not overprinted with []
	if multi_range == 'T':
		multi_list = process_multi_2(T, steps , multi_steps)
	else:
		T = step_split(T, steps)
	
	if multi_range == 'P':
		multi_list = process_multi_2(P, steps, multi_steps)
	else:
		P = step_split(P, steps)

	if multi_range == 'pH':
		multi_list = process_multi_2(pH, steps, multi_steps)
	else:
		pH = step_split(pH, steps)				#	note: this will be overridden if H+ is set to hetero EQ with a mineral
	
	if multi_range == 'fO2':
		multi_list = process_multi_2(fO2, steps, multi_steps)
	else:
		fO2 = step_split(fO2, steps) 			#	note: this will be overridden if fO2 is set to hetero EQ with a mineral



	### build species-element dictionary from data0. dict[key='basis species'] = ['associated element', stochiometry]
	basis_ele = determine_redox_ele_and_sp(data0, local_db_path)



	""" Build, run , and process outputs for all EQ3 files """
	m = 0 											#	multi_steps index
	while m < multi_steps:
		fail_list = [] 								#	Fail_list, keeps track of which steps have failed.
		vis_points = []								#	points to plot

		multi_step_points = multi_list[m]

		### Cycle through all steps (.3i runs), grabbing correct output data along the way
		s = 0
		while s < steps: 									
			### grab the current step 's' for all aq species
			aq_step = list_transpose(aq_sp_list, s)

			### build eq3.3i file: Change Call depending on what the multi_step is.
			if multi_range == 'T':
				build_input(multi_step_points[s], P[s], fO2[s], pH[s], charge_bal, aq_step, [], Hetero)

			elif multi_range == 'P':
				build_input(T[s], multi_step_points[s], fO2[s], pH[s], charge_bal, aq_step, [], Hetero)

			elif multi_range == 'fO2':
				build_input(T[s], P[s], multi_step_points[s], pH[s], charge_bal, aq_step, [], Hetero)

			elif multi_range == 'pH':
				build_input(T[s], P[s], fO2[s], multi_step_points[s], charge_bal, aq_step, [], Hetero)

			else: 		#	multi must be in aq
				build_input(T[s], P[s], fO2[s], pH[s], charge_bal, aq_step, multi_step_points[s], Hetero)


			### run the input file
			call_EQ3(data0, 'input.3i', s)

			### check for correct output and grab needed plot information from it.
			mine_output(fail_list, s, Hetero, x_axis, y_axis, vis_points, aq_sp_list, T, P, pH, fO2, basis_ele)

			### prep for next step
			try:
				os.remove('output')
				os.remove('pickup')
			except:
				continue
			s += 1

		### set x and y points
		x = [item[0] for item in vis_points]
		y = [item[1] for item in vis_points]

		### if this is the first step, set up plot
		if m == 0:
			font = {'weight':'bold',  'size': 22}
			matplotlib.rc('font', **font)							#	Set global font
			
			fig = plt.figure(figsize=(12,12))
			fig.set_frameon(False)
			fig.set_facecolor('white')
			ax = fig.add_subplot(111)
			x_label = set_x_label(basis_ele, x_axis) 
			y_label = set_y_label(basis_ele, y_axis)
			plt.xlabel(x_label)
			plt.ylabel(y_label)
			

			if multi_range in ['T', 'pH', 'P', 'fO2']:
				ax.plot(x, y, label=str(multi_range) + ' = ' + str(multi_step_points[0]))										#	Pretty Picture 1 !

			else:	#	if the multi_range was an aq species
				ax.plot(x, y, label=str(multi_step_points[0][0]) + ' = ' + str(multi_step_points[0][1]))										#	Pretty Picture 1 !

			ttl = grab_title(Hetero)								#	build title from hetero species onto this top plot
			plt.title(ttl)
		
		### plot all additional curves
		else:		#	if the multi_range was an aq species
			if multi_range in ['T', 'pH', 'P', 'fO2']:
				ax.plot(x, y, label=str(multi_range) + ' = ' + str(multi_step_points[0]))	
			else:
				ax.plot(x, y, label=str(multi_step_points[0][0]) + ' = ' + str(multi_step_points[0][1]))												#	Pretty Picture !

		m += 1

	### finish figure
	font = {'weight':'bold',  'size': 10}
	fig.text(0.95, 0.65, out_text) 							#	Add all other run information as text to plot
	plt.legend(bbox_to_anchor=(1.05, 0), loc=3, borderaxespad=0.)
	file_name = datetime.now().strftime("%Y%m%d-%H%M%S")	#	This ensures that all outputs have a unique name.
	fig.savefig(file_name, dpi=fig.dpi, bbox_inches="tight")#	Save that shit ! ! ! 		(bbox arguemnt forces the uinclusion of text outside the plot region in the saved file)
	plt.show()




#################################################################
#####################    Functions     ##########################
def input_error_check(T, P, pH, fO2, Aqueous, Hetero, steps, x_axis, y_axis, multi_range):
	### check T and P within data0 ranges. EQ3 shoudl provide this function, as it should warn the user when outside T/P range

	### check pH within 0 and 14

	### check that each hetero mineral is used only once
	unique_hetero = len(set([item[0] for item in Hetero]))
	if len([item for item in Hetero]) > unique_hetero:
		print('\n USER INPUT ERROR !\n')
		print('The same mineral cannot be used more than')
		print('once to set the equalibrium concentrations')
		print('of other systems components. IE, it cannot')
		print('control multiple aqueous activities.')
		sys.exit()



	### check for hetero control of H+,

	### check for hetero control of fO2
	###    If a mineral is supplied to EQ3 that cannot constrain O2, the input fails with
	###    the error:
	###     * Error - (EQ3NR/chkinx) The species O2(g) can't be constrained by
    ###      	the following reaction:

    ### check that multi_range is an item loaded into the system, that it has a range specified, and that it is not in the hetero_list

    ### check that multi_range is not assigned tot he x or y axis.




def out(T, P, pH, fO2, Aqueous, Hetero, x_axis):
	### supplies info text for the graph. Too include all set info other than x and y axis info.
	out_text = ('\n')
	
	if x_axis != 'T':
		if type(T) == list:
			out_text = out_text + 'T = ' + str(T[0]) + ' to ' + str(T[1]) + ' C \n'
		else:
			out_text = out_text + 'T = ' + str(T) + 'C \n'
	
	if x_axis != 'P':
		if type(P) == list:
			out_text = out_text + 'P = ' + str(P[0]) + ' to ' + str(P[1]) + ' bars \n'
		else:
			out_text = out_text + 'P = ' + str(P) + 'bars \n'
	
	if x_axis != 'pH' and 'H+' not in [item[1] for item in Hetero]:
		if type(pH) == list:
			out_text = out_text + 'pH = ' + str(pH[0]) + ' to ' + str(pH[1]) + '\n'
		else:
			out_text = out_text + 'pH = ' + str(pH) + '\n'
	
	if x_axis != 'fO2' and 'O2(g)' not in [item[1] for item in Hetero]:
		if type(fO2) == list:
			out_text = out_text + 'Log fO2 = ' + str(fO2[0]) + ' to ' + str(fO2[1]) + '\n'
		else:
			out_text = out_text + 'Log fO2 = ' + str(fO2) + '\n'

	for item in Aqueous:
		if type(item[1]) == list:
			out_text = out_text + str(item[0]) + ' = ' + str(item[1][0]) + ' to ' + str(item[1][1]) + ' molal\n'
		else:
			out_text = out_text + str(item[0]) + ' = ' + str(item[1]) + ' molal\n'

	return out_text

def step_split(val, steps):
	### Devide variables with ranges into the correct numeber of steps/values
	if type(val) == list:
		mn = float(min(val))
		mx = float(max(val))
		# step_size = (mx - mn) / float(steps)
		return list(np.linspace(mn, mx, steps))
	### Make point values a list of appropriate length
	else:
		return [float(val)]*steps

def process_lists(list, steps, multi_range, multi_steps):
	### Process a list of ['species_name', val/range] into [['species_name', val(step[s])], . . . ]
	new_list = []								#   new_list[x]= species,  new_list[x][y]= step for species
	multi_list = []
	for item in list:	
		if item[0] == multi_range:						#	if the species is set to multi_range, handle with multi_steps number 
			multi_list = process_multi(item, steps, multi_steps)
			
		else:
			rng = step_split(item[1], steps) 		#	make list of step values for given species
			nam = [item[0]]*steps 					#	make name equally long for weaving.
			sp = []									#	for speices build
			x = 0
			while x < len(rng):
				sp.append([nam[x], rng[x]])
				x += 1
			new_list.append(sp)						#	add species build to new_lst

	
	return new_list, multi_list

def process_multi(nam_val, steps, multi_steps):							
	### this function process the species with a range, specified as the multi_range. It takes ['sp name', [range]]
	### into the format:   [ [['sp_name', step_1_multistep_value]*len(steps)] ... [['sp_name', step_n_multistep_value]*len(steps)] ] 
	###	nam_val = ['sp_name', [range]]
	multi_list = [] 						#	multi_list = [[['sp name', step_value],  ... steps] ... multi_range].  multi_list[multistep][step]
	rng = step_split(nam_val[1], multi_steps) 	#	make list of step values for the multi_range species
	nam = [nam_val[0]]*steps   				#	make name equally long for weaving into steps.
	for val in rng: 					
		val = [val]*steps
		sp = []									#	for speices build
		x = 0
		while x < steps: 
			sp.append([nam[x], val[x]])
			x += 1
		multi_list.append(sp)						#	add species build to new_lst
	return multi_list

def process_multi_2(nam_val, steps, multi_steps):							
	### this function process the speices other than aq with a range. So instead of getting passed ['sp name', [range]], it is passed [range].
	### into the format:   [ [step_1_multistep_value*len(steps)] ... [step_n_multistep_value]*len(steps)] ] 
	###	nam_val = [range]
	multi_list = [] 								
	rng = step_split(nam_val, multi_steps)	 		#	make list of step values for the multi_range species
	for val in rng: 					
		val = [val]*steps
		multi_list.append(val)						#	add species build to new_list
	return multi_list

def list_transpose(list, s):
	### this simple converts list[x][y] to list[y][x]
	step_list = []
	for l in list:
		step_list.append(l[s])
	return step_list

def determine_redox_ele_and_sp(data0, local_db_path):
	### Determine the basis elemnts
	d0 = open(os.path.join(local_db_path, 'data0.') + data0, "r") 	#	open data0 for reading lines into memory
	d0_lines = d0.readlines()	 			#	READ THAT SHIT !
	d0.close() 								#	All Done


	### Find top of basis
	l = 0 														#	l = line index for data0 file
	while not re.findall('^basis species', d0_lines[l]):		#	stop if basis species are encountered
		l += 1
	l += 5 														#	skip past H2O in the first block, it is not needed here

	### build list of all basis species in data0, with associated element
	basis_ele = {} 												#	data0 dictionary {key=basis species: value=associated element}
	l += 1 														#	move to first basis (h2o) 
	while l < len(d0_lines):
		if re.findall('\+-----', d0_lines[l]): 					#	This should find the fist as well, as the x indexed begins on top of the +---- above h2o
			l += 1
			a = str(d0_lines[l])
			b = a.split()
			spe = b[0] 											#	basis species name of this block
			
			### Exit upon hitting the aqueous species block.
			if re.findall('^auxiliary', spe):
				l = len(d0_lines) 								#	skip to exit condition
			
			### deal with H2O, H+, and O2(g) . .  this is just for data0 mining. pH and fO2 as species will be dealt with elsewhere
			elif re.findall('^[hH]2[oO]$', spe) or re.findall('^[hH]\+$', spe) or re.findall('^[oO]2\([gG]\)$', spe):		#	Dont need these
				l += 1

			### Now insde a given species block.
			else:
				l += 1
				while not re.findall('\+-----', d0_lines[l]): 	#	stop if next species is reached
					if re.findall('element\(s\)', d0_lines[l]): #	data of interest is one line bloew this line
						l += 1
						a = str(d0_lines[l])
						b = a.split()
						c = [item for item in b[1::2] if item != 'H' and item != 'h' and item != 'O' and item != 'o'] 	#	for element that is not O or H, should only be 1, unless this is used on aux basis species
						d = re.search(c[0], a)
						e = d.start()
						sto = a[(e - 7) : (e - 1)] 				#	grab element stochiometry
						basis_ele[spe]= [c[0], sto] 			#	Add to dictionary
						l += 1
					else:
						l += 1

		else:
			l += 1


		### Add H+ and fO2 to the library as faux-species, for teh sake of heterogeneous EQ


	return basis_ele

def build_input(temp, press, fo2, pH, charge_bal, aq_step, multi_point, Hetero):
	with open("input.3i", "w") as f:
		
		### Set irdxc: -3 for O2 cotrolled by HeteroEQ, 0 for logfO2 raw entry.
		### The fO2lgi block can be left as is, is it is overwritten if O2(g) shows up
		### in the species block.
		if 'O2(g)' in [item[1] for item in Hetero]:
			irdxc = '-3'
		else:
			irdxc = ' 0'


		head = ('EQ3NR input file name=' + '\n' +
			'endit.' + '\n' +
			'* Special basis switches' + '\n' +
			'    nsbswt=   0' + '\n' +
			'* General' + '\n' +
			'     tempc=  ' + str(format_e(5, temp)) + '\n' +
			'    jpres3=   0' + '\n' +
			'     press=  ' + str(format_e(5, press)) + '\n' +
			'       rho=  1.02336E+00' + '\n' +
			'    itdsf3=   0' + '\n' +
			'    tdspkg=  0.00000E+00     tdspl=  0.00000E+00' + '\n' +
			'    iebal3=   1' + '\n' +
			'     uebal= ' + charge_bal + '\n' +
			'    irdxc3=  ' + irdxc + '\n' +
			'    fo2lgi= ' + str(format_e(5, fo2)) + '       ehi=  0.00000E+00' + '\n' +
			'       pei=  0.00000E+00    uredox= None' + '\n' +
			'* Aqueous basis species' + '\n'
			)
		f.write(head)

		### Add Aqueous species
		line_1 = 'species= '
		line_2 = '   jflgi=  0    covali=  ' 				#	0 for total_molality
		
		for item in aq_step: 								#	item[0] = name, item[1] = molality
			species_add = line_1 + item[0] + '\n' + line_2 + str(format_e(5, item[1])) + '\n'
			f.write(species_add) 

		### If the multi_range is an aq speices, add it here
		if multi_point:
			species_add = line_1 + multi_point[0] + '\n' + line_2 + str(format_e(5, multi_point[1])) + '\n'
			f.write(species_add) 


		### Add pH if it is not set as a hetero EQ species
		if 'H+' not in [item[1] for item in Hetero]: 		
			pH_add = line_1 + 'H+' + '\n' + '   jflgi= 20    covali=  ' + str(pH) + '\n'		
			f.write(pH_add)


		### Add heterogeneous EQ species
		if Hetero:																#	if here, to allow users to not use this function.
			line_2 = '   jflgi= 25    covali=  0.000' 
			line_3 = '  ucospi= ' 
			for item in Hetero: 
				species_add = line_1 + item[1] + '\n' + line_2 + '\n' + line_3 + item[0] + '\n'
				f.write(species_add) 


		tail = ('endit.' + '\n' +
			'* Ion exchangers' + '\n' +
			'    qgexsh=        F' + '\n' +
			'       net=   0' + '\n' +
			'* Ion exchanger compositions' + '\n' +
			'      neti=   0' + '\n' +
			'* Solid solution compositions' + '\n' +
			'      nxti=   0' + '\n' +
			'* Alter/suppress options' + '\n' +
			'     nxmod=   0' + '\n' +
			'* Iopt, iopg, iopr, and iodb options' + '\n' +
			'*               1    2    3    4    5    6    7    8    9   10' + '\n' +
			'  iopt1-10=     0    0    0    0    0    0    0    0    0    0' + '\n' +
			' iopt11-20=     0    0    0    0    0    0    0    0    0    0' + '\n' +
			'  iopg1-10=     0    0    0    0    0    0    0    0    0    0' + '\n' +
			' iopg11-20=     0    0    0    0    0    0    0    0    0    0' + '\n' +
			'  iopr1-10=     0    0    0    0    0    0    0    0    0    0' + '\n' +
			' iopr11-20=     0    0    0    0    0    0    0    0    0    0' + '\n' +
			'  iodb1-10=     2    0    2    2    0    0    0    0    0    0' + '\n' +
			' iodb11-20=     0    0    0    0    0    0    0    0    0    0' + '\n' +
			'* Numerical parameters' + '\n' +
			'     tolbt=  0.00000E+00     toldl=  0.00000E+00' + '\n' +
			'    itermx=   0' + '\n' +
			'* Ordinary basis switches' + '\n' +
			'    nobswt=   0' + '\n' +
			'* Saturation flag tolerance' + '\n' +
			'    tolspf=  0.00000E+00' + '\n' +
			'* Aqueous phase scale factor' + '\n' +
			'    scamas=  1.00000E+00' + '\n')
		f.write(tail)

		f.close()

def call_EQ3(l, f, s):
	### Call EQ3 with data0 suffix 'l' on input file 'f'
	# print('calling EQ3 on ', f, ' using ', l)
	### In order for the below call to work, the EQ3/6 configuration (scripts/__init__.py) must have been Imported.
	
	args = ['/bin/csh', '/Users/graysonboyer/SUPCRTandEQs/Automate_EQ3/bin/runeq3', l, f] 		#	Arguements to be run inside csh. ./bin/runeq3 data0_suffix .3i_file
	runs_script_and_wait(args, 1.0) 				#	call args

def mine_output(fail_list, s, Hetero, x_axis, y_axis, vis_points, aq_sp_list, T, P, pH, fO2, basis_ele):
			
	### Load output
	try:
		f = open('output', 'r')
		lines = f.readlines()
		f.close()
	except: 										#	if output is not present at all, note and exit.
		fail_list.append(s)
		print('No output generated on step ' + str(s) + ' Check input parameters, and restart the kernel before running again.')
		sys.exit()      								#	Thus upon the first failure the run will stop.


	### if there is a failure within output, note and exit to next step (s + 1).
	status = ''
	for line in lines:
		if re.findall('Hybrid Newton-Raphson iteration converged in', line):
			status = 'Complete'
	if status != 'Complete':
		print('The system as it is currently setup, has failed. Run EQ3_Error to evaluate the casue of the failure ')
		sys.exit()									#	Thus upon the first failure the run will stop.


	### Set y_axis info
	y = None 										#	set y value to null
	if y_axis == 'pH':								#	if pH
		l = 0 										#	l = line index
		while not re.findall(' NBS pH scale', lines[l]):
			l += 1
		a = str(lines[l])
		b = a.split()
		y = float(b[3])								#	Set y_axis value (3= pH)

	elif y_axis == 'O2(g)': 
		l = 0 										#	l = line index
		while not re.findall('^   O2\(g\)', lines[l]):
			l += 1
		a = str(lines[l])
		b = a.split()
		y = float(b[1])								#	Set y_axis value (1 = Logfo2)

	else:											#	y_axis is not pH or fO2
		y_spe = basis_ele[y_axis][0]				#	y_spe is the element of the aqueous component of the Mineral EQ (compoenent being solved for by EQ3), IE. Si for SiO2,AQ in EQ with Quartz
		
		### Find y_species values
		l = 0
		while not re.findall('--- Elemental Composition of the Aqueous Solution ---', lines[l]):
			l += 1
		l += 4 										#	first aq species
		while not re.findall('--- Numerical Composition of the Aqueous Solution ---', lines[l]):
			if re.findall('^ {5}' + y_spe + ' ', lines[l]):
				a = str(lines[l])
				b = a.split()
				y = np.log10(float(b[4]))			#	Set y_axis value (1= mg/L) (4=molality)
				l += 1
			else:
				l += 1
		if not y:									#	If y_spe was not found in the aq block (it is still None)
			y = 0.0 	 							#	Set it to 0.


	### Set x_axis info: doesnt need to be searched, as it was set by user, however note that if the range chosen for the x_axis is
	### an aq species, then it is the associated elemental abundance, not the basis species, which is set.
	x = None
	try:
		index = [item[0][0] for item in aq_sp_list].index(x_axis) 	#	find the index of x_axis varaible in the aq species list.
		x = aq_sp_list[index][s][1]									#	grab value for species from associated step s.

	except: 														#	if x_axis is not an aq species, then it must be in the reaction parameters. Set accordingly.
		if x_axis == 'T':
			x = T[s]		
		if x_axis == 'P':	
			x = P[s]
		if x_axis == 'pH':
			x = pH[s]
		if x_axis == 'fO2':
			x = fO2[s]

	### Add point to vis_points list
	vis_points.append([x, y])	

def set_x_label(basis_ele, x_axis):
	try:
		x_label = basis_ele[x_axis] 	#	call associated element of basis species form dict.
		x_label = x_label[0] + ' (molal)'

	except:								#	Range not an Aq species
		if x_axis == 'T':
			x_label = 'T (C)'		
		if x_axis == 'P':	
			x_label = 'P (bars)'
		if x_axis == 'pH':
			x_label = 'pH'
		if x_axis == 'fO2':
			x_label = 'Log(fO2)'
	return x_label

def set_y_label(basis_ele, y_axis):
	if y_axis == 'pH':
		y_label = y_axis
	elif y_axis == 'fO2' or y_axis == 'O2(g)':
		y_label = 'Log fO2'
	else:
		y_label = 'Log ' + basis_ele[y_axis][0] + ' (molal)'
	return y_label

def grab_title(Hetero):
	ttl = '\n'
	if Hetero:
		for item in Hetero:
			ttl = ttl + item[1] + ' in EQ with ' + item[0] + '\n'
		return ttl


#################################################################
#####################    Helper Functions     ###################


def read_inputs(file_type, location):
	### this function can find all .6o files in all downstream folders, hence the additio of file_list
	file_name = [] 						#	file names
	file_list = []						# 	file names with paths
	for root, dirs, files in os.walk(location):# this works. 
		for file in files:
			if file.endswith(file_type):
				file_name.append(file)
				file_list.append(os.path.join(root, file)) 					
	
	return file_name, file_list

def mk_check_del_directory(path):
	###  This code checks for the dir being created, and if it is already present, deletes it, before recreating it
	if not os.path.exists(path): 							#	Check if the dir is alrady pessent
		os.makedirs(path) 									#	Build desired output directory
	else:
		shutil.rmtree(path) 								#	Remove directory and contents if it is already present.
		os.makedirs(path)

def mk_check_del_file(path):
	###  This code checks for the file being created/moved already exists at the destination. And if so, delets it.
	if os.path.isfile(path): 								#	Check if the file is alrady pessent
		os.remove(path) 									#	Delete file

def ck_for_empty_file(f):
	if 	os.stat(f).st_size == 0:
		print('file: ' + str(f) + ' is empty.')
		sys.exit()

def runs_script_and_wait(args, wait_time):	
	with open(os.devnull, 'w') as fp: 								#	use of devnull will allow me to supress written output, and hopefull speed things up.
		Popen(args, stdout=fp).wait()								#	wait should give time for process to complete (I have ahd problems with this, specifically with EQ36, so a sleep time was added as well) 
	sleep(wait_time)

def format_e(p, n):
	#### n = number, p = precisions
    return "%0.*E"%(p,n)

#################################################################
####################     Error Handeling     ####################

